# subrion-template-agency

### Installation
1. Download latest release https://github.com/intelliants/subrion-template-agency/releases
2. Unpack contents of archive to `[root]/templates` folder
3. Rename `subrion-template-agency-master` to `agency`
4. Activate template in admin dashboard.

#### Or you can
```
cd [root]/templates # where root is the location of subrion core
git clone https://github.com/intelliants/subrion-template-agency.git ./agency
```
Voila! Just activate it in your Dashboard / Extensions / Templates