<?php /* Smarty version Smarty-3.1.19, created on 2016-05-03 11:52:21
         compiled from "/Applications/MAMP/htdocs/comics/templates/agency/layout.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16077899265728c93503f8f5-30935524%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f77a6a77744ce067ecf5ec719315f643dc7faeba' => 
    array (
      0 => '/Applications/MAMP/htdocs/comics/templates/agency/layout.tpl',
      1 => 1460448644,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16077899265728c93503f8f5-30935524',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'core' => 0,
    'key' => 0,
    'value' => 0,
    'img' => 0,
    'iaBlocks' => 0,
    'name' => 0,
    'item' => 0,
    'action' => 0,
    '_content_' => 0,
    'manageMode' => 0,
    'previewMode' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5728c9354628d0_42514266',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5728c9354628d0_42514266')) {function content_5728c9354628d0_42514266($_smarty_tpl) {?><?php if (!is_callable('smarty_function_ia_hooker')) include '/Applications/MAMP/htdocs/comics/includes/smarty/intelli_plugins/function.ia_hooker.php';
if (!is_callable('smarty_modifier_date_format')) include '/Applications/MAMP/htdocs/comics/includes/smarty/plugins/modifier.date_format.php';
if (!is_callable('smarty_function_randnum')) include '/Applications/MAMP/htdocs/comics/includes/smarty/intelli_plugins/function.randnum.php';
?><!DOCTYPE html>
<html lang="<?php echo $_smarty_tpl->tpl_vars['core']->value['language']['iso'];?>
" dir="<?php echo $_smarty_tpl->tpl_vars['core']->value['language']['direction'];?>
">
	<head>
		<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontBeforeHeadSection'),$_smarty_tpl);?>


		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<title><?php echo iaSmarty::ia_print_title(array(),$_smarty_tpl);?>
</title>
		<meta name="description" content="<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['meta-description'];?>
">
		<meta name="keywords" content="<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['meta-keywords'];?>
">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="generator" content="Subrion CMS - Open Source Content Management System">
		<meta name="robots" content="index">
		<meta name="robots" content="follow">
		<meta name="revisit-after" content="1 day">
		<base href="<?php echo @constant('IA_URL');?>
">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link rel="shortcut icon" href="<?php if (!empty($_smarty_tpl->tpl_vars['core']->value['config']['site_favicon'])) {?><?php echo $_smarty_tpl->tpl_vars['core']->value['page']['nonProtocolUrl'];?>
uploads/<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['site_favicon'];?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['core']->value['page']['nonProtocolUrl'];?>
favicon.ico<?php }?>">

		<?php echo iaSmarty::ia_add_media(array('files'=>'jquery, subrion, bootstrap','order'=>0),$_smarty_tpl);?>

		<?php echo iaSmarty::ia_print_js(array('files'=>'_IA_TPL_app','order'=>999),$_smarty_tpl);?>


		<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontAfterHeadSection'),$_smarty_tpl);?>


		<?php echo iaSmarty::ia_print_css(array('display'=>'on'),$_smarty_tpl);?>


		<?php $_smarty_tpl->smarty->_tag_stack[] = array('ia_add_js', array()); $_block_repeat=true; echo iaSmarty::ia_add_js(array(), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

			intelli.pageName = '<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['name'];?>
';

			<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['core']->value['customConfig']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
				intelli.config.<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
 = '<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
';
			<?php } ?>
		<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo iaSmarty::ia_add_js(array(), $_block_content, $_smarty_tpl, $_block_repeat); } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	</head>
	<body class="page-<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['name'];?>
">
		<header class="header"<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_bg']) {?> style="background-image: url('<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['nonProtocolUrl'];?>
uploads/<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_bg'];?>
');"<?php }?>>
			<nav class="navbar navbar-default">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						 <a class="navbar-brand page-scroll" href="#page-top">
							<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['enable_text_logo']) {?>
								<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['logo_text'];?>

							<?php } else { ?>
								<?php if (!empty($_smarty_tpl->tpl_vars['core']->value['config']['site_logo'])) {?>
									<img src="<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['nonProtocolUrl'];?>
uploads/<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['site_logo'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['site'];?>
">
								<?php } else { ?>
									<img src="<?php echo $_smarty_tpl->tpl_vars['img']->value;?>
logo.png" alt="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['site'];?>
">
								<?php }?>
							<?php }?>
						</a>
					</div>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<?php echo $_smarty_tpl->getSubTemplate ('language-selector.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

						<?php echo iaSmarty::ia_blocks(array('block'=>'account'),$_smarty_tpl);?>

						<?php echo iaSmarty::ia_blocks(array('block'=>'mainmenu'),$_smarty_tpl);?>

					</div>
				</div>
			</nav>
			<?php echo iaSmarty::ia_blocks(array('block'=>'teaser'),$_smarty_tpl);?>

		</header>
		<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontBeforeBreadcrumb'),$_smarty_tpl);?>

		<?php echo $_smarty_tpl->getSubTemplate ('breadcrumb.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<!-- PORTFOLIO -->
		<?php if (isset($_smarty_tpl->tpl_vars['iaBlocks']->value['verytop'])) {?>
			<div class="verytop">
				<div class="container"><?php echo iaSmarty::ia_blocks(array('block'=>'verytop'),$_smarty_tpl);?>
</div>
			</div>
		<?php }?>
		<!-- SERVICE -->
		<?php if ('index'==$_smarty_tpl->tpl_vars['core']->value['page']['name']) {?>
			<?php if (isset($_smarty_tpl->tpl_vars['iaBlocks']->value['service1'])||isset($_smarty_tpl->tpl_vars['iaBlocks']->value['service2'])||isset($_smarty_tpl->tpl_vars['iaBlocks']->value['service3'])) {?>
				<div class="verytop1">
					<div class="container">
						<?php echo iaSmarty::ia_blocks(array('block'=>"verytop1"),$_smarty_tpl);?>

						<div class="row">
							<div class="<?php echo iaSmarty::width(array('section'=>'verytop1','position'=>'service1','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'service1'),$_smarty_tpl);?>
</div>
							<div class="<?php echo iaSmarty::width(array('section'=>'verytop1','position'=>'service2','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'service2'),$_smarty_tpl);?>
</div>
							<div class="<?php echo iaSmarty::width(array('section'=>'verytop1','position'=>'service3','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'service3'),$_smarty_tpl);?>
</div>
						</div>
					</div>
				</div>
			<?php }?>
		<?php }?>
		<?php if ('index'!=$_smarty_tpl->tpl_vars['core']->value['page']['name']) {?>
			<div class="content">
				<div class="container">
					<div class="row">
						<div class="<?php echo iaSmarty::width(array('section'=>'content','position'=>'left','tag'=>'col-md-'),$_smarty_tpl);?>
 aside">
							<?php echo iaSmarty::ia_blocks(array('block'=>'left'),$_smarty_tpl);?>

						</div>
						<div class="<?php echo iaSmarty::width(array('section'=>'content','position'=>'center','tag'=>'col-md-'),$_smarty_tpl);?>
">
							<div class="content__wrap">
								<?php echo iaSmarty::ia_blocks(array('block'=>'top'),$_smarty_tpl);?>

								<div class="content__header">
									<h1><?php echo $_smarty_tpl->tpl_vars['core']->value['page']['title'];?>
</h1>
									<ul class="content__actions">
										<?php  $_smarty_tpl->tpl_vars['action'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['action']->_loop = false;
 $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['core']->value['actions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['action']->key => $_smarty_tpl->tpl_vars['action']->value) {
$_smarty_tpl->tpl_vars['action']->_loop = true;
 $_smarty_tpl->tpl_vars['name']->value = $_smarty_tpl->tpl_vars['action']->key;
?>
											<li>
												<?php if ('action-favorites'==$_smarty_tpl->tpl_vars['name']->value) {?>
													<?php echo iaSmarty::printFavorites(array('item'=>$_smarty_tpl->tpl_vars['item']->value,'itemtype'=>$_smarty_tpl->tpl_vars['item']->value['item'],'guests'=>true),$_smarty_tpl);?>

												<?php } else { ?>
													<a data-toggle="tooltip" title="<?php echo $_smarty_tpl->tpl_vars['action']->value['title'];?>
" <?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['action']->value['attributes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value) {
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?><?php echo $_smarty_tpl->tpl_vars['key']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['value']->value;?>
" <?php } ?>>
														<span class="fa fa-<?php echo $_smarty_tpl->tpl_vars['name']->value;?>
"></span>
													</a>
												<?php }?>
											</li>
										<?php } ?>
									</ul>
								</div>
								<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontBeforeNotifications'),$_smarty_tpl);?>

								<?php echo $_smarty_tpl->getSubTemplate ('notification.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

								<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontBeforeMainContent'),$_smarty_tpl);?>

								<div class="content__body">
									<?php echo $_smarty_tpl->tpl_vars['_content_']->value;?>

								</div>
								<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontAfterMainContent'),$_smarty_tpl);?>

								<?php echo iaSmarty::ia_blocks(array('block'=>'bottom'),$_smarty_tpl);?>

							</div>
						</div>
					</div>
				</div>
			</div>
		<?php }?>
		<!-- TEAM -->
		<?php if ('index'==$_smarty_tpl->tpl_vars['core']->value['page']['name']) {?>
			<?php if (isset($_smarty_tpl->tpl_vars['iaBlocks']->value['footer1'])||isset($_smarty_tpl->tpl_vars['iaBlocks']->value['footer2'])||isset($_smarty_tpl->tpl_vars['iaBlocks']->value['footer3'])) {?>
				<div class="footer-block">
					<?php echo iaSmarty::ia_blocks(array('block'=>'footer-block'),$_smarty_tpl);?>

					<div class="footer-blocks">
						<div class="container">
							<div class="row">
								<div class="<?php echo iaSmarty::width(array('section'=>'footer-blocks','position'=>'footer1','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'footer1'),$_smarty_tpl);?>
</div>
								<div class="<?php echo iaSmarty::width(array('section'=>'footer-blocks','position'=>'footer2','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'footer2'),$_smarty_tpl);?>
</div>
								<div class="<?php echo iaSmarty::width(array('section'=>'footer-blocks','position'=>'footer3','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'footer3'),$_smarty_tpl);?>
</div>
							</div>
						</div>
					</div>
				</div>
			<?php }?>
		<?php }?>
		<!-- ABOUT -->
		<?php if ('index'==$_smarty_tpl->tpl_vars['core']->value['page']['name']) {?>
			<?php if (isset($_smarty_tpl->tpl_vars['iaBlocks']->value['text1'])||isset($_smarty_tpl->tpl_vars['iaBlocks']->value['text2'])||isset($_smarty_tpl->tpl_vars['iaBlocks']->value['text3'])) {?>
				<div class="text-block">
					<div class="container"><?php echo iaSmarty::ia_blocks(array('block'=>'text-block'),$_smarty_tpl);?>
</div>
					<div class="text-blocks">
						<div class="container">
							<div class="row">
								<div class="<?php echo iaSmarty::width(array('section'=>'text-blocks','position'=>'text1','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'text1'),$_smarty_tpl);?>
</div>
								<div class="<?php echo iaSmarty::width(array('section'=>'text-blocks','position'=>'text2','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'text2'),$_smarty_tpl);?>
</div>
								<div class="<?php echo iaSmarty::width(array('section'=>'text-blocks','position'=>'text3','tag'=>'col-md-'),$_smarty_tpl);?>
"><?php echo iaSmarty::ia_blocks(array('block'=>'text3'),$_smarty_tpl);?>
</div>
							</div>
						</div>
					</div>
				</div>
			<?php }?>
		<?php }?>
		<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontAfterFooterLinks'),$_smarty_tpl);?>

		<!-- FOOTER -->
		<div class="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<p>&copy; <?php echo smarty_modifier_date_format($_SERVER['REQUEST_TIME'],'%Y');?>
 <?php echo iaSmarty::lang(array('key'=>'powered_by_subrion'),$_smarty_tpl);?>
</p>
					</div>
					<div class="col-md-4">
						<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social']) {?>
							<ul class="list-inline social-buttons">
								<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_t']) {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_t'];?>
" class="twitter"><span class="fa fa-twitter"></span></a></li><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_f']) {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_f'];?>
" class="facebook"><span class="fa fa-facebook"></span></a></li><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_g']) {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_g'];?>
" class="google-plus"><span class="fa fa-google-plus"></span></a></li><?php }?>
								<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['website_social_i']) {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['core']->value['config']['website_social_i'];?>
" class="linkedin"><span class="fa fa-linkedin"></span></a></li><?php }?>
							</ul>
						<?php }?>
					</div>
					<div class="col-md-4 footer-links">
						<?php echo iaSmarty::ia_blocks(array('block'=>'copyright'),$_smarty_tpl);?>

					</div>
				</div>
			</div>
		</div>
		<!-- SYSTEM STUFF -->
		<?php if ($_smarty_tpl->tpl_vars['core']->value['config']['cron']) {?>
			<div style="display: none;">
				<img src="<?php echo $_smarty_tpl->tpl_vars['core']->value['page']['nonProtocolUrl'];?>
cron/?<?php echo smarty_function_randnum(array(),$_smarty_tpl);?>
" width="1" height="1" alt="">
			</div>
		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['manageMode']->value)) {?>
			<?php echo $_smarty_tpl->getSubTemplate ('visual-mode.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['previewMode']->value)) {?>
			<p><?php echo iaSmarty::lang(array('key'=>'youre_in_preview_mode'),$_smarty_tpl);?>
</p>
		<?php }?>

		<?php echo iaSmarty::ia_print_js(array('display'=>'on'),$_smarty_tpl);?>


		<?php echo smarty_function_ia_hooker(array('name'=>'smartyFrontFinalize'),$_smarty_tpl);?>

	</body>
</html><?php }} ?>
