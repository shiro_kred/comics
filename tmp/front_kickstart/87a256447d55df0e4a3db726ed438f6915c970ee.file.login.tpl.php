<?php /* Smarty version Smarty-3.1.19, created on 2016-05-03 05:43:25
         compiled from "/Applications/MAMP/htdocs/comics/templates/common/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:753210764572872bd74a409-98061218%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '87a256447d55df0e4a3db726ed438f6915c970ee' => 
    array (
      0 => '/Applications/MAMP/htdocs/comics/templates/common/login.tpl',
      1 => 1455472436,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '753210764572872bd74a409-98061218',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'core' => 0,
    'name' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_572872bd855fe5_24327414',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_572872bd855fe5_24327414')) {function content_572872bd855fe5_24327414($_smarty_tpl) {?><div class="row">
	<div class="col-md-<?php if ($_smarty_tpl->tpl_vars['core']->value['providers']) {?>8<?php } else { ?>12<?php }?>">
		<form action="<?php echo @constant('IA_URL');?>
login/" method="post">
			<?php echo iaSmarty::preventCsrf(array(),$_smarty_tpl);?>


			<div class="form-group">
				<label for="field_login"><?php echo iaSmarty::lang(array('key'=>'username_or_email'),$_smarty_tpl);?>
:</label>
				<input class="form-control" type="text" tabindex="4" name="username" value="<?php if (isset($_POST['username'])) {?><?php echo htmlspecialchars($_POST['username'], ENT_QUOTES, 'UTF-8', true);?>
<?php }?>">
			</div>

			<div class="form-group">
				<label for="field_password"><?php echo iaSmarty::lang(array('key'=>'password'),$_smarty_tpl);?>
:</label>
				<input class="form-control" type="password" tabindex="5" name="password">
			</div>

			<div class="form-group">
				<div class="checkbox">
					<label><input type="checkbox" name="remember"> <?php echo iaSmarty::lang(array('key'=>'remember_me'),$_smarty_tpl);?>
</label>
				</div>
			</div>

			<div class="form-group form-actions">
				<a class="btn btn-link pull-right" href="<?php echo @constant('IA_URL');?>
forgot/"><?php echo iaSmarty::lang(array('key'=>'forgot'),$_smarty_tpl);?>
</a>
				<button class="btn btn-primary" type="submit" tabindex="6" name="login"><?php echo iaSmarty::lang(array('key'=>'login'),$_smarty_tpl);?>
</button>
				<a class="btn btn-link" href="<?php echo @constant('IA_URL');?>
registration/" rel="nofollow"><?php echo iaSmarty::lang(array('key'=>'registration'),$_smarty_tpl);?>
</a>
			</div>
		</form>
	</div>
	<?php if ($_smarty_tpl->tpl_vars['core']->value['providers']) {?>
		<div class="col-md-4">
			<div class="social-providers">
				<p><?php echo iaSmarty::lang(array('key'=>'login_with_social_network'),$_smarty_tpl);?>
:</p>
				<?php  $_smarty_tpl->tpl_vars['provider'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['provider']->_loop = false;
 $_smarty_tpl->tpl_vars['name'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['core']->value['providers']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['provider']->key => $_smarty_tpl->tpl_vars['provider']->value) {
$_smarty_tpl->tpl_vars['provider']->_loop = true;
 $_smarty_tpl->tpl_vars['name']->value = $_smarty_tpl->tpl_vars['provider']->key;
?>
					<a class="btn btn-block btn-social btn-<?php echo mb_strtolower($_smarty_tpl->tpl_vars['name']->value, 'UTF-8');?>
" href="<?php echo @constant('IA_URL');?>
login/<?php echo mb_strtolower($_smarty_tpl->tpl_vars['name']->value, 'UTF-8');?>
/"><span class="fa fa-<?php echo mb_strtolower($_smarty_tpl->tpl_vars['name']->value, 'UTF-8');?>
"></span> <?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</a>
				<?php } ?>
			</div>
		</div>
	<?php }?>
</div><?php }} ?>
