<?php /* Smarty version Smarty-3.1.19, created on 2016-05-03 01:33:40
         compiled from "/Applications/MAMP/htdocs/comics/templates/common/block.common-statistics.tpl" */ ?>
<?php /*%%SmartyHeaderCode:131600991557283834297aa5-93237004%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '711961bc37f60dc37db0690839fa8e4de1149d71' => 
    array (
      0 => '/Applications/MAMP/htdocs/comics/templates/common/block.common-statistics.tpl',
      1 => 1455472436,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '131600991557283834297aa5-93237004',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'common_statistics' => 0,
    'group' => 0,
    'data' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_572838342fb2f7_16285269',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_572838342fb2f7_16285269')) {function content_572838342fb2f7_16285269($_smarty_tpl) {?><?php  $_smarty_tpl->tpl_vars['data'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['data']->_loop = false;
 $_smarty_tpl->tpl_vars['group'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['common_statistics']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['data']->key => $_smarty_tpl->tpl_vars['data']->value) {
$_smarty_tpl->tpl_vars['data']->_loop = true;
 $_smarty_tpl->tpl_vars['group']->value = $_smarty_tpl->tpl_vars['data']->key;
?>
	<table class="table table-condensed table-striped statistics">
		<thead>
		<tr>
			<th colspan="2"><?php echo iaSmarty::lang(array('key'=>$_smarty_tpl->tpl_vars['group']->value),$_smarty_tpl);?>
</th>
		</tr>
		</thead>
		<tbody>
			<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
				<tr>
					<td><?php echo $_smarty_tpl->tpl_vars['item']->value['title'];?>
:</td>
					<td><?php echo $_smarty_tpl->tpl_vars['item']->value['value'];?>
</td>
				</tr>
			<?php } ?>
		</tbody>
		<tfoot>
		<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			<?php if (isset($_smarty_tpl->tpl_vars['item']->value['html'])) {?>
				<tr>
					<td colspan="2"><div class="user-list"><?php echo $_smarty_tpl->tpl_vars['item']->value['html'];?>
</div></td>
				</tr>
			<?php }?>
		<?php } ?>
		</tfoot>
	</table>
<?php } ?><?php }} ?>
