<?php /* Smarty version Smarty-3.1.19, created on 2016-05-03 11:57:01
         compiled from "/Applications/MAMP/htdocs/comics/plugins/elfinder/templates/admin/ckeditor.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19819415355728ca4de84be7-93523880%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0c3e90214bfe6943158a69b8e509ccc09bc9ce0a' => 
    array (
      0 => '/Applications/MAMP/htdocs/comics/plugins/elfinder/templates/admin/ckeditor.tpl',
      1 => 1455472436,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19819415355728ca4de84be7-93523880',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5728ca4e0036f0_68676855',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5728ca4e0036f0_68676855')) {function content_5728ca4e0036f0_68676855($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>elFinder Upload Manager</title>

	<!-- jQuery and jQuery UI (REQUIRED) -->
	<link rel="stylesheet" type="text/css" href="<?php echo @constant('IA_CLEAR_URL');?>
plugins/elfinder/js/jqueryui/jquery-ui.min.css">
	<script src="<?php echo @constant('IA_CLEAR_URL');?>
js/jquery/jquery.js"></script>
	<script src="<?php echo @constant('IA_CLEAR_URL');?>
plugins/elfinder/js/jqueryui/jquery-ui.min.js"></script>

	<!-- elFinder CSS (REQUIRED) -->
	<link rel="stylesheet" type="text/css" href="<?php echo @constant('IA_CLEAR_URL');?>
plugins/elfinder/includes/elfinder/css/elfinder.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo @constant('IA_CLEAR_URL');?>
plugins/elfinder/includes/elfinder/css/theme.css">

	<!-- elFinder JS (REQUIRED) -->
	<script src="<?php echo @constant('IA_CLEAR_URL');?>
plugins/elfinder/includes/elfinder/js/elfinder.min.js"></script>

	<!-- elFinder initialization (REQUIRED) -->
	<script type="text/javascript" charset="utf-8">
		// Helper function to get parameters from the query string.
		function getUrlParam(paramName) {
			var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i') ;
			var match = window.location.search.match(reParam) ;

			return (match && match.length > 1) ? match[1] : '' ;
		}

		$().ready(function() {
			var funcNum = getUrlParam('CKEditorFuncNum');

			var elf = $('#elfinder').elfinder({
				url : '<?php echo @constant('IA_ADMIN_URL');?>
elfinder/read.json',
				getFileCallback : function(file) {
					window.opener.CKEDITOR.tools.callFunction(funcNum, file.url);
					window.close();
				},
				resizable: false
			}).elfinder('instance');
		});
	</script>
</head>
<body>
	<!-- Element where elFinder will be created (REQUIRED) -->
	<div id="elfinder"></div>
</body>
</html><?php }} ?>
